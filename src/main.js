import { createApp } from 'vue'
import App from './App.vue'
import vue3GoogleLogin from 'vue3-google-login'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store/index.js'
import router from './router/index.js'

const app = createApp(App)

app.use(vue3GoogleLogin, {
  clientId: '541656281005-upj385kcj74ag2jvuo28pm2grl8k4m66.apps.googleusercontent.com',
  scope: 'email profile',
})
app.use(store)
app.use(VueAxios, axios)
app.use(router)
app.mount('#app')
