import { createRouter,createWebHistory } from "vue-router";

import LandingPage from '../components/LandingPage.vue';
import FileUpload from '../components/FileUpload.vue';
import StatusPage from '../components/StatusPage.vue';
import ReportPage from '../components/ReportPage.vue';

const routes = [
    {
        path:"/",
        name: "LandingPage",
        component: LandingPage,
    },
    {
        path:"/upload-file",
        name: "FileUpload",
        component: FileUpload,
    },
    {
        path:"/status",
        name: "StatusPage",
        component: StatusPage,
    },
    {
        path:"/report",
        name: "ReportPage",
        component: ReportPage,
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})
export default router;