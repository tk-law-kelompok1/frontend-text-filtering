import {createStore} from 'vuex'

export default createStore({
    state: {
        accessToken:'',
        isLoggedin: false,
        userProfile:null,
        client:null,
        gLibraryLoaded:null,
        client_id: "541656281005-upj385kcj74ag2jvuo28pm2grl8k4m66.apps.googleusercontent.com"
    },
    getters: {
        getAccessToken(state){
            return state.accessToken
        },
        getIsLoggedin(state){
            return state.isLoggedin
        },
        getUserProfile(state){
            return state.userProfile
        },
        getClient(state){
            return state.client
        },
        getGLibraryLoaded(state){
            return state.gLibraryLoaded
        },
        getClientId(state){
            return state.client_id
        }
    },
    mutations:{
        SET_ACCESS_TOKEN(state, newToken){
            state.accessToken = newToken
        },
        SET_IS_LOGGEDIN(state, newStatus){
            state.isLoggedin = newStatus
        },
        SET_USER_PROFILE(state, userProfile){
            state.userProfile = userProfile
        },
        SET_CLIENT(state, client){
            state.client = client
        },
        SET_LIBRARY_LOADED(state, libraryLoaded){
            state.gLibraryLoaded = libraryLoaded
        }
    },
    actions:{
        setAccessToken({commit}, newToken){
            commit("SET_ACCESS_TOKEN", newToken)
        },
        setIsLoggedin({commit},newStatus){
            commit("SET_IS_LOGGEDIN",newStatus)
        },
        setUserProfile({commit},userProfile){
            commit("SET_USER_PROFILE",userProfile)
        },
        setClient({commit},client){
            commit("SET_CLIENT",client)
        },
        setGLibraryLoaded({commit},libraryLoaded){
            commit("SET_LIBRARY_LOADED",libraryLoaded)
        }
    }
})
